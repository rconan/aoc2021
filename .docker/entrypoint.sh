#!/bin/bash
set -euo pipefail

if [[ -f "src/bin/${1}.rs" ]]; then
    cargo build --release --bin "${1}"
    time "target/release/${1}" < "input/${1}"
fi
