use std::io;
use std::io::BufRead;
use std::mem::swap;
use std::num::ParseIntError;
use itertools::Itertools;
use thiserror::Error;
use crate::util::CountingSet;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Internal Error: {0}")]
    InternalError(&'static str),
    #[error(transparent)]
    IOError(#[from] io::Error),
    #[error(transparent)]
    ParseIntError(#[from] ParseIntError),
    #[error("InsufficientInput")]
    InsufficientInput,
    #[error("ParseError")]
    ParseError(String),
    #[error("Empty Template")]
    EmptyTemplate,
}

#[derive(Debug)]
pub struct Polymer {
    first: char,
    pairs: CountingSet<(char, char)>,
    rules: [char; 26usize.pow(2)],
}

impl Polymer {
    fn new() -> Self {
        Self{
            first: '\0',
            pairs: CountingSet::default(),
            rules: ['\0'; 26usize.pow(2)],
        }
    }

    pub fn from_input(input: impl BufRead) -> Result<Self, Error> {
        let mut lines = input.lines();
        let mut p = Self::new();
        let template_line = lines.next().ok_or(Error::InsufficientInput)??;
        p.first = template_line.chars().next().ok_or(Error::EmptyTemplate)?;
        for pair in template_line.chars().tuple_windows() {
            p.pairs.increment(pair);
        }
        lines.next();  // Ignore the blank line
        for l in lines {
            let line = l?;
            let mut pair = 0usize;
            for (i, part) in line.split(" -> ").enumerate() {
                match i {
                    0 => pair = Self::str_index(part)?,
                    1 => p.rules[pair] = part.chars().next().ok_or(Error::ParseError(part.to_string()))?,
                    _ => return Err(Error::ParseError(part.to_string()))
                }
            }
        }
        Ok(p)
    }

    pub fn step(&mut self) -> Result<(), Error> {
        let mut new = CountingSet::default();
        for ((a, b), freq) in self.pairs.iter() {
            let mid = self.rules[Self::tuple_index(*a, *b)?];
            new.increment_by((*a, mid), *freq);
            new.increment_by((mid, *b), *freq);
        }
        swap(&mut self.pairs, &mut new);
        Ok(())
    }

    pub fn frequency_range(&self) -> Result<(usize, usize), Error> {
        let mut frequencies = [0; 26];
        frequencies[Self::char_index(self.first)?] += 1;
        for ((_, c), q) in self.pairs.iter() {
            frequencies[Self::char_index(*c)?] += q;
        }
        let mut min = usize::MAX;
        let mut max = 0;
        for f in frequencies.iter() {
            if *f != 0 && *f < min {
                min = *f;
            }
            if *f > max {
                max = *f;
            }
        }
        Ok((min, max))
    }

    fn char_index(c: char) -> Result<usize, Error> {
        Ok(c.to_digit(36).ok_or(Error::InternalError("Invalid Character"))? as usize - 10)
    }

    fn str_index(pair: &str) -> Result<usize, Error> {
        let mut index = 0;
        for (i, char) in pair.chars().enumerate() {
            if i >= 2 {
                return Err(Error::ParseError(pair.to_string()))
            }
            index += (char.to_digit(36).ok_or(Error::ParseError(pair.to_string()))? - 10) * 26u32.pow(i as u32);
        }
        Ok(index as usize)
    }

    fn tuple_index(a: char, b: char) -> Result<usize, Error> {
        let msd = b.to_digit(36).ok_or(Error::InternalError("Invalid Character"))? - 10;
        let lsd = a.to_digit(36).ok_or(Error::InternalError("Invalid Character"))? - 10;
        Ok((msd * 26 + lsd) as usize)
    }
}