use std::collections::VecDeque;
use thiserror::Error;
use std::{fmt, io};

#[derive(Error,Debug)]
pub enum Error {
    #[error(transparent)]
    IOError(#[from] io::Error),
    #[error("Too much input")]
    InputOverflow,
    #[error("Unexpected Character: {0}")]
    UnexpectedCharacter(char),
}

#[derive(Default)]
pub struct Grid {
    energy: [[u32; 10]; 10],
    flashes: usize,
}

impl fmt::Display for Grid {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for j in 0..self.energy.len() {
            for i in 0..self.energy[j].len() {
                write!(f, "{}", self.energy[j][i])?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

impl Grid {
    pub fn from_input(input: impl io::BufRead) -> Result<Self, Error> {
        let mut g = Grid::default();
        for (j, line) in input.lines().enumerate() {
            if j >= g.energy.len() {
                return Err(Error::InputOverflow)
            }
            for (i, char) in line?.chars().enumerate() {
                if i >= g.energy[j].len() {
                    return Err(Error::InputOverflow)
                }
                g.energy[j][i] = char.to_digit(10).ok_or(Error::UnexpectedCharacter(char))?;
            }
        }
        Ok(g)
    }

    pub fn step(&mut self) -> usize {
        let old_flashes = self.flashes;
        let mut flash_queue = VecDeque::new();
        for j in 0..self .energy.len() {
            for i in 0..self.energy[j].len() {
                self.energy[j][i] += 1;
                if self.energy[j][i] == 10 {
                    flash_queue.push_back((j, i));
                    self.flashes += 1;
                }
            }
        }
        while let Some((j, i)) = flash_queue.pop_front() {
            for (dj, di) in [(-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1)] {
                let (mj, mi): (isize, isize) = (j as isize + dj, i as isize + di);
                if mj < 0 || mj >= self.energy.len() as isize {
                    continue
                }
                let cj = mj as usize;
                if mi < 0 || mi >= self.energy[cj].len() as isize {
                    continue
                }
                let ci = mi as usize;
                self.energy[cj][ci] += 1;
                if self.energy[cj][ci] == 10 {
                    flash_queue.push_back((cj, ci));
                    self.flashes += 1;
                }
            }
        }
        for j in 0..self .energy.len() {
            for i in 0..self.energy[j].len() {
                if self.energy[j][i] > 9 {
                    self.energy[j][i] = 0;
                }
            }
        }
        self.flashes - old_flashes
    }

    pub fn flashes(&self) -> usize {
        self.flashes
    }
}