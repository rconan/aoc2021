use std::collections::BinaryHeap;
use fxhash::FxHashSet;
use std::io;
use thiserror::Error;

#[derive(Error,Debug)]
pub enum Error {
    #[error(transparent)]
    IOError(#[from] io::Error),
    #[error("Unexpected character")]
    UnexpectedCharacter,
}

#[derive(Default)]
pub struct Cave {
    grid: Vec<Vec<usize>>,
    minima: Vec<(usize, usize)>,
}

impl Cave {
    pub fn from_input(input: impl io::BufRead) -> Result<Self, Error> {
        let mut c = Cave::default();
        for maybe_line in input.lines() {
            let line = maybe_line?;
            let mut row = vec![0; line.len()];
            for (column, maybe_height) in line.chars().map(|c| c.to_digit(10)).enumerate() {
                row[column] = maybe_height.ok_or(Error::UnexpectedCharacter)? as usize;
            }
            c.grid.push(row);
        }
        c.calculate_minima();
        Ok(c)
    }

    fn calculate_minima(&mut self) {
        let max_row = self.grid.len() - 1;
        let max_column = self.grid[0].len() - 1;
        for j in 0..=max_row {
            for i in 0..=max_column {
                let height = self.grid[j][i];
                if
                    (j == 0 || height < self.grid[j-1][i]) &&
                    (j == max_row || height < self.grid[j+1][i]) &&
                    (i == 0 || height < self.grid[j][i-1]) &&
                    (i == max_column || height < self.grid[j][i+1])
                {
                    self.minima.push((i, j));
                }
            }
        }
    }

    pub fn risk_sum(&self) -> usize {
        let mut risk = 0;
        for (i, j) in &self.minima {
            risk += 1 + self.grid[*j][*i];
        }
        risk
    }

    fn basin_points(&self, (i, j): (usize, usize), excludes: &mut FxHashSet<(usize, usize)>) -> usize {
        let mut count = 1;
        excludes.insert((i, j));
        for (di, dj) in [
            (0, -1),
            (0, 1),
            (-1, 0),
            (1, 0),
        ] {
            let (ci, cj) = ((i as isize + di), (j as isize + dj));
            if
                cj >= 0 && cj < self.grid.len() as isize &&
                ci >= 0 && ci < self.grid[j].len() as isize &&
                !excludes.contains(&(ci as usize, cj as usize)) &&
                self.grid[cj as usize][ci as usize] < 9 &&
                self.grid[cj as usize][ci as usize] >= self.grid[j][i]
            {
                count += self.basin_points((ci as usize, cj as usize), excludes);
            }
        }
        count
    }

    pub fn basins(&self) -> usize {
        let mut basin_sizes = BinaryHeap::new();
        for minimum in &self.minima {
            basin_sizes.push(self.basin_points(*minimum, &mut FxHashSet::default()));
        }
        let mut ret = 1;
        for size in basin_sizes.iter().take(3) {
            ret *= size;
        }
        ret
    }
}