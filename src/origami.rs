use std::collections::VecDeque;
use fxhash::FxHashMap;
use std::io;
use std::io::BufRead;
use std::num::ParseIntError;
use bitvec::prelude::*;
use itertools::izip;
use lazy_static::lazy_static;
use thiserror::Error;

lazy_static! {
    static ref ALPHABET: FxHashMap<[BitVec; 6], char> = {
        let mut alphabet = FxHashMap::default();
        alphabet.insert([
            bits![0,1,1,0,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
            bits![1,1,1,1,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
        ], 'A');
        alphabet.insert([
            bits![1,1,1,0,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
            bits![1,1,1,0,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
            bits![1,1,1,0,0].to_bitvec(),
        ], 'B');
        alphabet.insert([
            bits![0,1,1,0,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
            bits![1,0,0,0,0].to_bitvec(),
            bits![1,0,0,0,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
            bits![0,1,1,0,0].to_bitvec(),
        ], 'C');
        alphabet.insert([
            bits![1,1,1,0,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
            bits![1,1,1,0,0].to_bitvec(),
        ], 'D');
        alphabet.insert([
            bits![1,1,1,1,0].to_bitvec(),
            bits![1,0,0,0,0].to_bitvec(),
            bits![1,1,1,0,0].to_bitvec(),
            bits![1,0,0,0,0].to_bitvec(),
            bits![1,0,0,0,0].to_bitvec(),
            bits![1,1,1,1,0].to_bitvec(),
        ], 'E');
        alphabet.insert([
            bits![1,1,1,1,0].to_bitvec(),
            bits![1,0,0,0,0].to_bitvec(),
            bits![1,1,1,0,0].to_bitvec(),
            bits![1,0,0,0,0].to_bitvec(),
            bits![1,0,0,0,0].to_bitvec(),
            bits![1,0,0,0,0].to_bitvec(),
        ], 'F');
        alphabet.insert([
            bits![0,1,1,0,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
            bits![1,0,0,0,0].to_bitvec(),
            bits![1,0,1,1,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
            bits![0,1,1,1,0].to_bitvec(),
        ], 'G');
        alphabet.insert([
            bits![1,0,0,1,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
            bits![1,1,1,1,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
        ], 'H');
        alphabet.insert([
            bits![0,1,1,1,0].to_bitvec(),
            bits![0,0,1,0,0].to_bitvec(),
            bits![0,0,1,0,0].to_bitvec(),
            bits![0,0,1,0,0].to_bitvec(),
            bits![0,0,1,0,0].to_bitvec(),
            bits![0,1,1,1,0].to_bitvec(),
        ], 'I');
        alphabet.insert([
            bits![0,0,1,1,0].to_bitvec(),
            bits![0,0,0,1,0].to_bitvec(),
            bits![0,0,0,1,0].to_bitvec(),
            bits![0,0,0,1,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
            bits![0,1,1,0,0].to_bitvec(),
        ], 'J');
        alphabet.insert([
            bits![1,0,0,1,0].to_bitvec(),
            bits![1,0,1,0,0].to_bitvec(),
            bits![1,1,0,0,0].to_bitvec(),
            bits![1,0,1,0,0].to_bitvec(),
            bits![1,0,1,0,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
        ], 'K');
        alphabet.insert([
            bits![1,0,0,0,0].to_bitvec(),
            bits![1,0,0,0,0].to_bitvec(),
            bits![1,0,0,0,0].to_bitvec(),
            bits![1,0,0,0,0].to_bitvec(),
            bits![1,0,0,0,0].to_bitvec(),
            bits![1,1,1,1,0].to_bitvec(),
        ], 'L');
        alphabet.insert([
            bits![1,0,0,0,1].to_bitvec(),
            bits![1,1,0,0,1].to_bitvec(),
            bits![1,0,1,0,1].to_bitvec(),
            bits![1,0,1,0,1].to_bitvec(),
            bits![1,0,0,1,1].to_bitvec(),
            bits![1,0,0,0,1].to_bitvec(),
        ], 'N');
        alphabet.insert([
            bits![0,1,1,0,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
            bits![0,1,1,0,0].to_bitvec(),
        ], 'O');
        alphabet.insert([
            bits![1,1,1,0,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
            bits![1,1,1,0,0].to_bitvec(),
            bits![1,0,0,0,0].to_bitvec(),
            bits![1,0,0,0,0].to_bitvec(),
        ], 'P');
        alphabet.insert([
            bits![1,1,1,0,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
            bits![1,1,1,0,0].to_bitvec(),
            bits![1,0,1,0,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
        ], 'R');
        alphabet.insert([
            bits![0,1,1,1,0].to_bitvec(),
            bits![1,0,0,0,0].to_bitvec(),
            bits![1,0,0,0,0].to_bitvec(),
            bits![0,1,1,0,0].to_bitvec(),
            bits![0,0,0,1,0].to_bitvec(),
            bits![1,1,1,0,0].to_bitvec(),
        ], 'S');
        alphabet.insert([
            bits![1,0,0,1,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
            bits![1,0,0,1,0].to_bitvec(),
            bits![0,1,1,0,0].to_bitvec(),
        ], 'U');
        alphabet.insert([
            bits![1,0,0,0,1].to_bitvec(),
            bits![0,1,0,1,0].to_bitvec(),
            bits![0,0,1,0,0].to_bitvec(),
            bits![0,0,1,0,0].to_bitvec(),
            bits![0,0,1,0,0].to_bitvec(),
            bits![0,0,1,0,0].to_bitvec(),
        ], 'Y');
        alphabet.insert([
            bits![1,1,1,1,0].to_bitvec(),
            bits![0,0,0,1,0].to_bitvec(),
            bits![0,0,1,0,0].to_bitvec(),
            bits![0,1,0,0,0].to_bitvec(),
            bits![1,0,0,0,0].to_bitvec(),
            bits![1,1,1,1,0].to_bitvec(),
        ], 'Z');
        alphabet
    };
}

#[derive(Debug,Error)]
pub enum Error {
    #[error("Internal Error: {0}")]
    InternalError(&'static str),
    #[error(transparent)]
    IOError(#[from] io::Error),
    #[error(transparent)]
    ParseIntError(#[from] ParseIntError),
    #[error("Coordinate Syntax Error")]
    CoordinateSyntaxError,
    #[error("Instruction Syntax Error")]
    InstructionSyntaxError,
    #[error("No Points")]
    NoPoints,
    #[error("No Instruction")]
    NoInstruction,
    #[error("Wrong Height For Reading: {0}")]
    WrongHeight(usize),
    #[error("Unrecognised Character")]
    NoCharacter,
}

enum Axis {
    X,
    Y,
}

#[derive(Default)]
pub struct Sheet {
    points: Vec<BitVec>,
    instructions: VecDeque<(Axis, usize)>,
}

impl Sheet {
    pub fn from_input(input: impl BufRead) -> Result<Self, Error> {
        let mut s = Sheet::default();
        let mut lines = input.lines();
        for l in &mut lines {
            let line = l?;
            if line == "" {
                break;
            }
            let mut parts = line.split(',');
            let i = parts.next().ok_or(Error::CoordinateSyntaxError)?.parse::<usize>()?;
            let j = parts.next().ok_or(Error::CoordinateSyntaxError)?.parse::<usize>()?;
            if j >= s.points.len() {
                s.points.resize_with(j + 1, BitVec::new)
            }
            let row = s.points.get_mut(j).ok_or(Error::InternalError("Unable to get row vector"))?;
            if i >= row.len() {
                row.resize(i + 1, false);
            }
            row.set(i, true);
        }
        for l in lines {
            let line = l?;
            let mut instruction_parts = line.split(' ').nth(2).ok_or(Error::InstructionSyntaxError)?.split('=');
            let axis = match instruction_parts.next().ok_or(Error::InstructionSyntaxError)? {
                "x" => Axis::X,
                "y" => Axis::Y,
                _ => return Err(Error::InstructionSyntaxError),
            };
            let value = instruction_parts.next().ok_or(Error::InstructionSyntaxError)?.parse::<usize>()?;
            s.instructions.push_back((axis, value));
        }
        Ok(s)
    }

    pub fn do_fold(&mut self) -> Result<(), Error> {
        let (axis, value) = self.instructions.pop_front().ok_or(Error::NoInstruction)?;
        match axis {
            Axis::X => self.do_fold_x(value),
            Axis::Y => self.do_fold_y(value),
        }
    }

    fn do_fold_y(&mut self, value: usize) -> Result<(), Error> {
        let (keep, discard) = self.points.split_at_mut(value + 1);
        for (j, row) in discard.iter().enumerate() {
            let target_row = keep.get_mut(value - j - 1).ok_or(Error::InternalError("Unable to find target row"))?;
            for i in row.iter_ones() {
                if i >= target_row.len() {
                    target_row.resize(i + 1, false);
                }
                target_row.set(i, true);
            }
        }
        self.points.truncate(value);
        Ok(())
    }

    fn do_fold_x(&mut self, value: usize) -> Result<(), Error> {
        for row in self.points.iter_mut() {
            for i in value+1..row.len() {
                if row[i] {
                    row.set(2 * value - i, true);
                }
            }
            row.truncate(value);
        }
        Ok(())
    }

    pub fn do_folds(&mut self) -> Result<(), Error> {
        while !self.instructions.is_empty() {
            self.do_fold()?;
        }
        Ok(())
    }

    pub fn pad(&mut self) -> Result<(), Error> {
        let max_width = self.points.iter().map(|bv| bv.len()).max().ok_or(Error::NoPoints)?;
        let target_width = (max_width as f64 / 5f64).ceil() as usize * 5;
        for row in self.points.iter_mut() {
            row.resize(target_width, false);
        }
        Ok(())
    }

    pub fn count_points(&self) -> usize {
        self.points.iter().map(|r| r.count_ones()).sum()
    }

    pub fn print(&self) {
        for row in &self.points {
            for bit in row {
                print!("{}", if *bit { '#' } else { '.' });
            }
            println!();
        }
    }

    pub fn read_letters(&self) -> Result<String, Error> {
        if self.points.len() != 6 {
            return Err(Error::WrongHeight(self.points.len()))
        }
        let mut s = String::new();
        for (a,b,c,d,e,f) in izip!(
            self.points[0].chunks(5),
            self.points[1].chunks(5),
            self.points[2].chunks(5),
            self.points[3].chunks(5),
            self.points[4].chunks(5),
            self.points[5].chunks(5),
        ) {
            let char = ALPHABET.get(&[
                a.to_bitvec(),
                b.to_bitvec(),
                c.to_bitvec(),
                d.to_bitvec(),
                e.to_bitvec(),
                f.to_bitvec(),
            ]).ok_or(Error::NoCharacter)?;
            s += &*char.to_string();
        }
        Ok(s)
    }
}