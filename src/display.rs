use fxhash::FxHashMap;

use lazy_static::lazy_static;
use thiserror::Error;

#[derive(Error,Debug)]
pub enum Error {
    #[error("Malformed Input")]
    MalformedInput
}

lazy_static! {
    static ref LOOKUP_TABLE: FxHashMap<[usize; 8], usize> = {
        let mut lt = FxHashMap::default();
        lt.insert([0,0,0,1,0,3,3,1], 0);
        lt.insert([0,0,0,0,1,1,3,1], 1);
        lt.insert([0,0,1,1,1,2,2,1], 2);
        lt.insert([0,0,0,0,1,3,2,1], 3);
        lt.insert([0,0,0,0,0,1,2,1], 4);
        lt.insert([0,0,1,1,1,2,3,1], 5);
        lt.insert([0,0,0,0,0,3,3,1], 6);
        lt
    };
}

#[derive(Default)]
pub struct Display {
    numbers: FxHashMap<[bool; 7], usize>,
}

const A: usize = 'a' as usize;

impl Display {
    pub fn from_permutations_string(line: &str) -> Result<Self, Error> {
        let permutations = line.split(' ');
        let d = Self::from_permutations(permutations)?;
        Ok(d)
    }

    fn from_permutations<'a>(permutations: impl Iterator<Item=&'a str>) -> Result<Self, Error> {
        let mut d = Self::default();
        let mut segment_counter = [[0; 8]; 7];
        for p in permutations {
            let length = p.len();
            for c in p.chars() {
                let segment_index = c as usize - A;
                segment_counter[segment_index][length] += 1;
            }
        }
        let mut mapping = [0; 7];
        for (i, s) in segment_counter.iter().enumerate() {
            mapping[LOOKUP_TABLE[s]] = i;
        }
        for (number, segments) in [
            [true, true, true, false, true, true, true],     // 0
            [false, false, true, false, false, true, false], // 1
            [true, false, true, true, true, false, true],    // 2
            [true, false, true, true, false, true, true],    // 3
            [false, true, true, true, false, true, false],   // 4
            [true, true, false, true, false, true, true],    // 5
            [true, true, false, true, true, true, true],     // 6
            [true, false, true, false, false, true, false],  // 7
            [true, true, true, true, true, true, true],      // 8
            [true, true, true, true, false, true, true],     // 9
        ].iter().enumerate() {
            let mut number_segments = [false; 7];
            for (segment, enabled) in segments.iter().enumerate() {
                 number_segments[mapping[segment]] = *enabled;
            }
            d.numbers.insert(number_segments, number);
        }
        Ok(d)
    }

    pub fn read(&self, value: &str) -> Result<usize, Error> {
        let mut output = 0;
        for digit in value.split(' ') {
            let mut segments = [false; 7];
            for c in digit.chars() {
                segments[c as usize - A] = true;
            }
            output *= 10;
            output += self.numbers[&segments];
        }
        Ok(output)
    }

    pub fn count_1478(value: &str) -> usize {
        let mut output = 0;
        for digit in value.split(' ') {
            match digit.len() {
                2 | 3 | 4 | 7 => {
                    output += 1;
                },
                _ => { },
            }
        }
        output
    }
}
