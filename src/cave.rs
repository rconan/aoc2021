use fxhash::FxHashMap;
use std::{cmp, io};
use std::fmt::{Display, Formatter};
use std::io::BufRead;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error {
    #[error(transparent)]
    IOError(#[from] io::Error),
    #[error("Parse Error {0}")]
    ParseError(char),
}

#[derive(Debug,Default)]
pub struct Map {
    risks: Vec<Vec<usize>>,
}

impl Display for Map {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        for row in &self.risks {
            for n in row {
                write!(f, "{}", n)?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

impl Map {
    pub fn from_input(input: impl BufRead) -> Result<Self, Error> {
        let mut m = Self::default();
        for l in input.lines() {
            let mut row = Vec::new();
            let line = l?;
            for i in line.chars().map(|c| c.to_digit(10).ok_or(Error::ParseError(c))) {
                row.push(i? as usize);
            }
            m.risks.push(row);
        }
        Ok(m)
    }

    pub fn five_times(self) -> Self {
        let mut new = Self::default();
        for mut row in self.risks {
            let column_count = row.len();
            for repeat in 1..5 {
                for index in 0..column_count {
                    row.push(Self::increment(row[index], repeat));
                }
            }
            new.risks.push(row);
        }
        let row_count = new.risks.len();
        for repeat in 1..5 {
            for index in 0..row_count {
                let mut new_row = Vec::with_capacity(new.risks[index].len());
                for value in &new.risks[index] {
                    new_row.push(Self::increment(*value, repeat));
                }
                new.risks.push(new_row);
            }
        }
        new
    }

    fn increment(n: usize, i: usize) -> usize {
        (n + i - 1) % 9 + 1
    }

    pub fn lowest_risk(&self) -> usize {
        let mut lowest_risks = FxHashMap::default();
        self.populate_risk(&mut lowest_risks, 0, 0);
        cmp::min(lowest_risks[&(0, 1)], lowest_risks[&(1, 0)])
    }

    fn populate_risk(&self, lowest_risks: &mut FxHashMap<(usize, usize), usize>, i: usize, j: usize) {
        let risk = match (i == self.risks[j].len() - 1, j == self.risks.len() - 1) {
            (true, true) => {
                self.risks[j][i]
            },
            (true, false) => {
                if lowest_risks.get(&(i, j + 1)) == None {
                    self.populate_risk(lowest_risks, i, j + 1);
                }
                self.risks[j][i] + lowest_risks[&(i, j + 1)]
            },
            (false, true) => {
                if lowest_risks.get(&(i + 1, j)) == None {
                    self.populate_risk(lowest_risks, i + 1, j);
                }
                self.risks[j][i] + lowest_risks[&(i + 1, j)]
            },
            (false, false) => {
                if lowest_risks.get(&(i + 1, j)) == None {
                    self.populate_risk(lowest_risks, i + 1, j);
                }
                if lowest_risks.get(&(i, j + 1)) == None {
                    self.populate_risk(lowest_risks, i, j + 1);
                }
                let right = lowest_risks[&(i + 1, j)];
                let down = lowest_risks[&(i, j + 1)];
                if right < down {
                    let this_path_risk = self.risks[j][i] + right;
                    let backtrack_risk = self.risks[j + 1][i] + this_path_risk;
                    if backtrack_risk < lowest_risks[&(i, j + 1)] {
                        self.update_risk(lowest_risks, i, j + 1, backtrack_risk);
                    }
                    this_path_risk
                } else {
                    let this_path_risk = self.risks[j][i] + down;
                    let backtrack_risk = self.risks[j][i + 1] + this_path_risk;
                    if backtrack_risk < lowest_risks[&(i + 1, j)] {
                        self.update_risk(lowest_risks, i + 1, j, backtrack_risk);
                    }
                    this_path_risk
                }
            },
        };
        lowest_risks.insert((i, j), risk);
    }

    fn update_risk(&self, lowest_risks: &mut FxHashMap<(usize, usize), usize>, i: usize, j: usize, v: usize) {
        lowest_risks.insert((i, j), v);
        for (ii, jj) in [
            (i as isize - 1, j as isize),
            (i as isize + 1, j as isize),
            (i as isize, j as isize - 1),
            (i as isize, j as isize + 1),
        ].iter().filter(|(ii, jj)| {
            *jj >= 0 && *jj < self.risks.len() as isize &&
            *ii >= 0 && *ii < self.risks[*jj as usize].len() as isize
        }).map(|(ii, jj)| (*ii as usize, *jj as usize)) {
            if let Some(prev) = lowest_risks.get(&(ii, jj)) {
                let candidate = v + self.risks[jj][ii];
                if candidate < *prev {
                    self.update_risk(lowest_risks, ii, jj, candidate);
                }
            }
        }
    }
}