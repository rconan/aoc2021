use mimalloc::MiMalloc;

#[global_allocator]
static GLOBAL: MiMalloc = MiMalloc;

pub mod bingo;
pub mod bits;
pub mod cave;
pub mod caves;
pub mod crabs;
pub mod depth;
pub mod display;
pub mod fish;
pub mod lava;
pub mod octopodi;
pub mod origami;
pub mod polymer;
pub mod snailfish;
pub mod submarine;
pub mod syntax;
pub mod trench;
pub mod vents;

pub mod util;
