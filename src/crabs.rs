use std::io;
use std::num::ParseIntError;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error {
    #[error(transparent)]
    IOError(#[from] io::Error),
    #[error(transparent)]
    ParseIntError(#[from] ParseIntError),
    #[error("Insufficient Input")]
    InsufficientInput,
    #[error("Odd Number of Crabs")]
    OddNumber,
}

#[derive(Debug, Default)]
pub struct Crabs {
    positions: Vec<usize>,
}

pub enum Mode {
    Linear,
    IncrementalFloor,
    IncrementalCeiling,
}

impl Crabs {
    pub fn from_input(input: impl io::BufRead) -> Result<Self, Error> {
        let mut c = Self::default();
        for f in input.lines().next().ok_or(Error::InsufficientInput)??.split(',').map(|t| t.parse::<usize>()) {
            c.positions.push(f?);
        }
        Ok(c)
    }

    fn mean(&self, ceiling: bool) -> usize {
        let mean = self.positions.iter().sum::<usize>() as f64 / self.positions.len() as f64;
        let rounded = if ceiling { mean.ceil() } else { mean.floor() };
        rounded as usize
    }

    fn median(&self) -> Result<usize, Error> {
        let mut sorted = self.positions.clone();
        sorted.sort();
        let l = sorted.len();
        match l % 2 {
            0 => Ok(sorted[l / 2] as usize),
            _ => Err(Error::OddNumber),
        }
    }

    pub fn alignment_fuel(&self, mode: Mode) -> Result<usize, Error> {
        let avg = match mode {
            Mode::Linear => self.median()?,
            Mode::IncrementalFloor => self.mean(false),
            Mode::IncrementalCeiling => self.mean(true),
        } as isize;
        let mut fuel = 0;
        for p in &self.positions {
            let distance = (*p as isize - avg).abs();
            fuel += match mode {
                Mode::Linear => distance,
                _ => (distance * (distance + 1)) / 2,
            } as usize;
        }
        Ok(fuel)
    }
}