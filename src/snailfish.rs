use std::io;
use std::iter::Sum;
use std::num::ParseIntError;
use pest::Parser;
use pest_derive::Parser;
use std::ops::Add;
use pest::iterators::Pair;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error(transparent)]
    IOError(#[from] io::Error),
    #[error(transparent)]
    ParseIntError(#[from] ParseIntError),
    #[error(transparent)]
    ParseError(#[from] pest::error::Error<Rule>),
}

#[derive(Parser)]
#[grammar = "snailfish.pest"]
struct NumberParser;

#[derive(Clone,Debug)]
pub enum Number {
    Regular(usize),
    Pair(Box<Self>, Box<Self>),
}

impl PartialEq<Self> for Number {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Regular(a), Self::Regular(b)) => {
                *a == *b
            },
            (Self::Pair(l1, r1), Self::Pair(l2, r2)) => {
                l1 == l2 && r1 == r2
            },
            _ => false,
        }
    }
}

impl Add for Number {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        let mut new = Self::Pair(
            Box::new(self),
            Box::new(rhs),
        );
        new.reduce();
        new
    }
}

impl Sum for Number {
    fn sum<I: Iterator<Item=Self>>(mut iter: I) -> Self {
        let mut sum = iter.next().unwrap();
        for n in iter {
            sum = sum + n;
        }
        sum
    }
}

impl Number {
    pub fn from_str(s: &str) -> Result<Self, Error> {
        let mut pairs = NumberParser::parse(Rule::number, s)?;
        Ok(Self::from_pair(pairs.next().unwrap())?)
    }

    fn from_pair(p: Pair<Rule>) -> Result<Self, Error> {
        match p.as_rule() {
            Rule::regular => Ok(Self::Regular(p.as_str().parse()?)),
            Rule::pair => {
                let mut inner = p.into_inner();
                Ok(Self::Pair(
                    Box::new(Self::from_pair(inner.next().unwrap())?),
                    Box::new(Self::from_pair(inner.next().unwrap())?),
                ))
            },
            r => panic!("Unexpected Rule {:?}", r),
        }
    }

    pub fn magnitude(&self) -> usize {
        match self {
            Self::Regular(n) => *n,
            Self::Pair(l, r) => 3 * l.magnitude() + 2 * r.magnitude(),
        }
    }

    fn reduce(&mut self) {
        loop {
            if self.maybe_explode(0).0 {
                continue;
            }
            if self.maybe_split() {
                continue;
            }
            break;
        }
    }

    fn increase_leftmost(&mut self, v: usize) -> Option<usize> {
        match self {
            Self::Regular(n) => {
                *n += v;
                None
            },
            Self::Pair(left, right) => match left.increase_leftmost(v) {
                Some(vv) => right.increase_leftmost(vv),
                None => None,
            }
        }
    }

    fn increase_rightmost(&mut self, v: usize) -> Option<usize> {
        match self {
            Self::Regular(n) => {
                *n += v;
                None
            },
            Self::Pair(left, right) => match right.increase_rightmost(v) {
                Some(vv) => left.increase_rightmost(vv),
                None => None,
            }
        }
    }

    fn maybe_explode(&mut self, level: usize) -> (bool, Option<usize>, Option<usize>) {
        if let Self::Pair(left, right) = self {
            match level {
                4 => {
                    match (left.as_ref(), right.as_ref()) {
                        (Self::Regular(l), Self::Regular(r)) => {
                            let (exploded_left, exploded_right) = (*l, *r);
                            *self = Self::Regular(0);
                            return (true, Some(exploded_left), Some(exploded_right))
                        }
                        _ => panic!("Exploding a pair of pairs")
                    }
                },
                _ => {
                    let (exploded, exploded_left, exploded_right) = left.maybe_explode(level + 1);
                    if exploded {
                        return match (right.as_mut(), exploded_right) {
                            (Self::Regular(r), Some(er)) => {
                                *r += er;
                                (exploded, exploded_left, None)
                            },
                            (r, Some(er)) => {
                                (exploded, exploded_left, r.increase_leftmost(er))
                            },
                            _ => {
                                (exploded, exploded_left, exploded_right)
                            },
                        }
                    }
                    let (exploded, exploded_left, exploded_right) = right.maybe_explode(level + 1);
                    if exploded {
                        return match (left.as_mut(), exploded_left) {
                            (Self::Regular(l), Some(el)) => {
                                *l += el;
                                (exploded, None, exploded_right)
                            },
                            (l, Some(el)) => {
                                (exploded, l.increase_rightmost(el), exploded_right)
                            },
                            _ => {
                                (exploded, exploded_left, exploded_right)
                            },
                        }
                    }
                },
            }
        }
        (false, None, None)
    }

    fn maybe_split(&mut self) -> bool {
        match self {
            Self::Regular(n) => {
                if *n >= 10 {
                    let half = *n as f64 / 2.0;
                    *self = Self::Pair(
                        Box::new(Self::Regular(half.floor() as usize)),
                        Box::new(Self::Regular(half.ceil() as usize)),
                    );
                    true
                } else {
                    false
                }
            },
            Self::Pair(left, right) => {
                left.maybe_split() || right.maybe_split()
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse() -> Result<(), Error> {
        assert_eq!(Number::from_str("[1,2]")?, Number::Pair(Box::new(Number::Regular(1)), Box::new(Number::Regular(2))));
        Ok(())
    }

    #[test]
    fn test_maybe_explode_no_left() {
        let mut n = Number::Pair(
            Box::new(Number::Pair(
                Box::new(Number::Pair(
                    Box::new(Number::Pair(
                        Box::new(Number::Pair(
                            Box::new(Number::Regular(9)),
                            Box::new(Number::Regular(8)),
                        )),
                        Box::new(Number::Regular(1)),
                    )),
                    Box::new(Number::Regular(2)),
                )),
                Box::new(Number::Regular(3)),
            )),
            Box::new(Number::Regular(4)),
        );
        let t = Number::Pair(
            Box::new(Number::Pair(
                Box::new(Number::Pair(
                    Box::new(Number::Pair(
                        Box::new(Number::Regular(0)),
                        Box::new(Number::Regular(9)),
                    )),
                    Box::new(Number::Regular(2)),
                )),
                Box::new(Number::Regular(3)),
            )),
            Box::new(Number::Regular(4)),
        );
        assert_eq!(n.maybe_explode(0), (true, Some(9), None));
        assert_eq!(n, t);
    }

    #[test]
    fn test_maybe_explode_multiple() {
        let mut n = Number::Pair(
            Box::new(Number::Pair(
                Box::new(Number::Regular(3)),
                Box::new(Number::Pair(
                    Box::new(Number::Regular(2)),
                    Box::new(Number::Pair(
                        Box::new(Number::Regular(1)),
                        Box::new(Number::Pair(
                            Box::new(Number::Regular(7)),
                            Box::new(Number::Regular(3)),
                        )),
                    )),
                )),
            )),
            Box::new(Number::Pair(
                Box::new(Number::Regular(6)),
                Box::new(Number::Pair(
                    Box::new(Number::Regular(5)),
                    Box::new(Number::Pair(
                        Box::new(Number::Regular(4)),
                        Box::new(Number::Pair(
                            Box::new(Number::Regular(3)),
                            Box::new(Number::Regular(2)),
                        )),
                    )),
                )),
            )),
        );
        let t = Number::Pair(
            Box::new(Number::Pair(
                Box::new(Number::Regular(3)),
                Box::new(Number::Pair(
                    Box::new(Number::Regular(2)),
                    Box::new(Number::Pair(
                        Box::new(Number::Regular(8)),
                        Box::new(Number::Regular(0)),
                    )),
                )),
            )),
            Box::new(Number::Pair(
                Box::new(Number::Regular(9)),
                Box::new(Number::Pair(
                    Box::new(Number::Regular(5)),
                    Box::new(Number::Pair(
                        Box::new(Number::Regular(4)),
                        Box::new(Number::Pair(
                            Box::new(Number::Regular(3)),
                            Box::new(Number::Regular(2)),
                        )),
                    )),
                )),
            )),
        );
        assert_eq!(n.maybe_explode(0), (true, None, None));
        assert_eq!(n, t);
    }

    #[test]
    fn test_maybe_explode() {
        let mut n = Number::Pair(
            Box::new(Number::Pair(
                Box::new(Number::Regular(6)),
                Box::new(Number::Pair(
                    Box::new(Number::Regular(5)),
                    Box::new(Number::Pair(
                        Box::new(Number::Regular(4)),
                        Box::new(Number::Pair(
                            Box::new(Number::Regular(3)),
                            Box::new(Number::Regular(2)),
                        )),
                    )),
                )),
            )),
            Box::new(Number::Regular(1)),
        );
        let t = Number::Pair(
            Box::new(Number::Pair(
                Box::new(Number::Regular(6)),
                Box::new(Number::Pair(
                    Box::new(Number::Regular(5)),
                    Box::new(Number::Pair(
                        Box::new(Number::Regular(7)),
                        Box::new(Number::Regular(0)),
                    )),
                )),
            )),
            Box::new(Number::Regular(3)),
        );
        assert_eq!(n.maybe_explode(0), (true, None, None));
        assert_eq!(n, t);
    }

    #[test]
    fn test_maybe_split() {
        let mut n = Number::Pair(
            Box::new(Number::Pair(
                Box::new(Number::Pair(
                    Box::new(Number::Pair(
                        Box::new(Number::Regular(0)),
                        Box::new(Number::Regular(7)),
                    )),
                    Box::new(Number::Regular(4)),
                )),
                Box::new(Number::Pair(
                    Box::new(Number::Regular(15)),
                    Box::new(Number::Pair(
                        Box::new(Number::Regular(0)),
                        Box::new(Number::Regular(13)),
                    )),
                )),
            )),
            Box::new(Number::Pair(
                Box::new(Number::Regular(1)),
                Box::new(Number::Regular(1)),
            )),
        );
        let t = Number::Pair(
            Box::new(Number::Pair(
                Box::new(Number::Pair(
                    Box::new(Number::Pair(
                        Box::new(Number::Regular(0)),
                        Box::new(Number::Regular(7)),
                    )),
                    Box::new(Number::Regular(4)),
                )),
                Box::new(Number::Pair(
                    Box::new(Number::Pair(
                        Box::new(Number::Regular(7)),
                        Box::new(Number::Regular(8)),
                    )),
                    Box::new(Number::Pair(
                        Box::new(Number::Regular(0)),
                        Box::new(Number::Regular(13)),
                    )),
                )),
            )),
            Box::new(Number::Pair(
                Box::new(Number::Regular(1)),
                Box::new(Number::Regular(1)),
            )),
        );
        assert_eq!(n.maybe_split(), true);
        assert_eq!(n, t);
    }

    #[test]
    fn test_reduce() {
        let mut n = Number::Pair(
            Box::new(Number::Pair(
                Box::new(Number::Pair(
                    Box::new(Number::Pair(
                        Box::new(Number::Pair(
                            Box::new(Number::Regular(4)),
                            Box::new(Number::Regular(3)),
                        )),
                        Box::new(Number::Regular(4)),
                    )),
                    Box::new(Number::Regular(4)),
                )),
                Box::new(Number::Pair(
                    Box::new(Number::Regular(7)),
                    Box::new(Number::Pair(
                        Box::new(Number::Pair(
                            Box::new(Number::Regular(8)),
                            Box::new(Number::Regular(4)),
                        )),
                        Box::new(Number::Regular(9)),
                    )),
                )),
            )),
            Box::new(Number::Pair(
                Box::new(Number::Regular(1)),
                Box::new(Number::Regular(1)),
            )),
        );
        let t = Number::Pair(
            Box::new(Number::Pair(
                Box::new(Number::Pair(
                    Box::new(Number::Pair(
                        Box::new(Number::Regular(0)),
                        Box::new(Number::Regular(7)),
                    )),
                    Box::new(Number::Regular(4)),
                )),
                Box::new(Number::Pair(
                    Box::new(Number::Pair(
                        Box::new(Number::Regular(7)),
                        Box::new(Number::Regular(8)),
                    )),
                    Box::new(Number::Pair(
                        Box::new(Number::Regular(6)),
                        Box::new(Number::Regular(0)),
                    )),
                )),
            )),
            Box::new(Number::Pair(
                Box::new(Number::Regular(8)),
                Box::new(Number::Regular(1)),
            )),
        );
        n.reduce();
        assert_eq!(n, t);
    }
}