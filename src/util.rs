use std::{
    collections::{
        hash_map::Iter,
    },
    hash::Hash,
};
use fxhash::FxHashMap;
use bitvec::vec::BitVec;

#[derive(Default, Debug)]
pub struct CountingSet<T> {
    map: FxHashMap<T, usize>,
}

impl<T> CountingSet<T> where T: Eq + Hash {
    pub fn decrement(&mut self, item: &T) -> Option<usize> {
        self.decrement_by(item, 1)
    }

    pub fn decrement_by(&mut self, item: &T, q: usize) -> Option<usize> {
        let value = self.map.get_mut(item)?;
        *value -= q;
        Some(*value)
    }

    pub fn increment(&mut self, item: T) -> usize {
        self.increment_by(item, 1)
    }

    pub fn increment_by(&mut self, item: T, q: usize) -> usize {
        let entry = self.map.entry(item).or_insert(0);
        *entry += q;
        *entry
    }

    pub fn get(&self, item: &T) -> usize {
        match self.map.get(item) {
            Some(v) => *v,
            None => 0,
        }
    }

    pub fn iter(&self) -> Iter<T, usize> {
        self.map.iter()
    }
}

#[derive(Default)]
pub struct BitPlane {
    vec: BitVec,
}

impl BitPlane {
    fn coordinate_to_index(x: usize, y: usize) -> usize {
        (2_usize.pow(x as u32) * (2 * y + 1) - 1) as usize
    }

    pub fn set(&mut self, x: usize, y: usize, v: bool) {
        let i = Self::coordinate_to_index(x, y);
        if i >= self.vec.len() {
            self.vec.resize(i + 1, false);
        }
        self.vec.set(i, v);
    }

    pub fn get(&self, x: usize, y: usize) -> bool {
        let i = Self::coordinate_to_index(x, y);
        if i < self.vec.len() {
            self.vec[i]
        } else {
            false
        }
    }
}