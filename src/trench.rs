use std::io;
use std::io::BufRead;
use std::num::ParseIntError;
use lazy_static::lazy_static;
use regex::Regex;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error(transparent)]
    IOError(#[from] io::Error),
    #[error(transparent)]
    ParseIntError(#[from] ParseIntError),
}

#[derive(Default)]
pub struct Trench {
    pub min_x: isize,
    pub max_x: isize,
    pub min_y: isize,
    pub max_y: isize,
}

lazy_static! {
    static ref PATTERN: Regex = Regex::new(r"target area: x=(-?\d+)\.\.(-?\d+), y=(-?\d+)\.\.(-?\d+)").unwrap();
}

impl Trench {
    pub fn from_input(mut input: impl BufRead) -> Result<Self, Error> {
        let mut s = String::new();
        input.read_line(&mut s)?;
        let parts = PATTERN.captures(&s).unwrap();
        let min_x = parts.get(1).unwrap().as_str().parse::<isize>()?;
        let max_x = parts.get(2).unwrap().as_str().parse::<isize>()?;
        let min_y = parts.get(3).unwrap().as_str().parse::<isize>()?;
        let max_y = parts.get(4).unwrap().as_str().parse::<isize>()?;
        Ok(Self{
            min_x,
            max_x,
            min_y,
            max_y,
        })
    }

    pub fn peak_height(&self, launch_x: isize, launch_y: isize) -> Option<isize> {
        let mut x = 0;
        let mut y = 0;
        let mut vx = launch_x;
        let mut vy = launch_y;
        let mut peak = isize::MIN;
        while x <= self.max_x && y >= self.min_y {
            if y > peak {
                peak = y;
            }
            if x >= self.min_x && y <= self.max_y {
                return Some(peak)
            }
            x += vx;
            y += vy;
            match vx {
                k if k > 0 => vx -= 1,
                k if k < 0 => vx += 1,
                _ => {},
            }
            vy -= 1;
        }
        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_peak_height() {
        let t = Trench{
            min_x: 20,
            max_x: 30,
            min_y: -10,
            max_y: -5,
        };
        assert_eq!(t.peak_height(7, 2), Some(3));
        assert_eq!(t.peak_height(6, 3), Some(6));
        assert_eq!(t.peak_height(9, 0), Some(0));
        assert_eq!(t.peak_height(17, -4), None);
        assert_eq!(t.peak_height(6, 9), Some(45));
    }
}