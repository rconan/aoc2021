use fxhash::{FxHashMap, FxHashSet};
use std::io;
use std::io::BufRead;
use std::num::ParseIntError;
use petgraph::graph::{NodeIndex, UnGraph};
use rayon::prelude::*;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error {
    #[error(transparent)]
    IOError(#[from] io::Error),
    #[error(transparent)]
    ParseIntError(#[from] ParseIntError),
    #[error("Syntax Error: {0}")]
    SyntaxError(String),
    #[error("Unexpected Character")]
    UnexpectedCharacter,
    #[error("Cave Name Too Long")]
    TooLong,
    #[error("No Start")]
    NoStart,
    #[error("Unknown Node")]
    UnknownNode,
    #[error("Empty Name")]
    EmptyName,
}

#[derive(Default)]
pub struct System {
    graph: UnGraph<String, ()>,
    indexes: FxHashMap<String, NodeIndex>,
}

impl System {
    pub fn from_input(input: impl BufRead) -> Result<Self, Error> {
        let mut s = System::default();
        for l in input.lines() {
            let line = l?;
            let mut edge_parts = line.split('-');
            let start_index = s.ensure_node(edge_parts.next().ok_or(Error::SyntaxError(line.to_string()))?);
            let end_index = s.ensure_node(edge_parts.next().ok_or(Error::SyntaxError(line.to_string()))?);
            s.graph.add_edge(start_index, end_index, ());
        }
        Ok(s)
    }

    fn ensure_node(&mut self, s: &str) -> NodeIndex {
        *self.indexes.entry(s.to_string()).or_insert_with(|| self.graph.add_node(s.to_string()))
    }

    fn paths_from(&self, i: NodeIndex, visited: &FxHashSet<NodeIndex>, repeat_small: bool) -> Result<usize, Error> {
        match self.graph.node_weight(i) {
            None => Err(Error::UnknownNode),
            Some(s) => {
                if s == "end" {
                    Ok(1)
                } else {
                    let mut new_visited = visited.clone();
                    let mut new_repeat_small = repeat_small;
                    if s.chars().next().ok_or(Error::EmptyName)?.is_lowercase() {
                        if visited.contains(&i) {
                            if s != "start" && repeat_small {
                                new_repeat_small = false
                            } else {
                                return Ok(0)
                            }
                        }
                        new_visited.insert(i);
                    }
                    let neighbours = self.graph.neighbors(i).collect::<Vec<NodeIndex>>();
                    let counts = neighbours.par_iter().map(|n| self.paths_from(*n, &new_visited, new_repeat_small)).collect::<Vec<Result<usize, Error>>>();
                    let mut count = 0;
                    for c in counts {
                        count += c?;
                    }
                    Ok(count)
                }
            },
        }
    }

    pub fn path_count(&self, repeat_small: bool) -> Result<usize, Error> {
        let start = *self.indexes.get("start").ok_or(Error::NoStart)?;
        let paths = self.paths_from(start, &FxHashSet::default(), repeat_small)?;
        Ok(paths)
    }
}