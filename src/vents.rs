use std::io;
use std::num::ParseIntError;
use thiserror::Error;
use crate::util::CountingSet;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Internal Error {0}")]
    InternalError(&'static str),
    #[error(transparent)]
    IOError(#[from] io::Error),
    #[error(transparent)]
    ParseIntError(#[from] ParseIntError),
    #[error("Parse Error")]
    ParseError,
    #[error("Misaligned line: ({0},{1}) -> ({2},{3})")]
    MisalignedLine(isize, isize, isize, isize)
}

#[derive(Default)]
pub struct VentMap {
    lines: Vec<((isize, isize), (isize, isize))>,
}

impl VentMap {
    pub fn from_input(input: impl io::BufRead) -> Result<Self, Error> {
        let mut m = Self::default();
        for l in input.lines() {
            let line = l?;
            let mut coordinate_parts = line.split(" -> ");
            let mut coordinate_1_parts = coordinate_parts.next().ok_or(Error::ParseError)?.split(',');
            let x1 = coordinate_1_parts.next().ok_or(Error::ParseError)?.parse()?;
            let y1 = coordinate_1_parts.next().ok_or(Error::ParseError)?.parse()?;
            let mut coordinate_2_parts = coordinate_parts.next().ok_or(Error::ParseError)?.split(',');
            let x2 = coordinate_2_parts.next().ok_or(Error::ParseError)?.parse()?;
            let y2 = coordinate_2_parts.next().ok_or(Error::ParseError)?.parse()?;
            if x2 - x1 != 0 && y2 - y1 != 0 && ((x2 - x1) as isize).abs() != ((y2 - y1) as isize).abs() {
                return Err(Error::MisalignedLine(x1, y1, x2, y2))
            }
            m.lines.push(((x1, y1), (x2, y2)));
        }
        Ok(m)
    }

    pub fn intersections(&self, diagonal: bool) -> usize {
        let mut counter = CountingSet::default();
        let mut intersections = 0;
        for ((mut x1, mut y1), (x2, y2)) in &self.lines {
            let (dx, dy) = (Self::unit_value(*x2 - x1), Self::unit_value(*y2 - y1));
            if !diagonal && dx != 0 && dy != 0 {
                continue
            }
            while x1 != x2 + dx || y1 != y2 + dy {
                if counter.increment((x1, y1)) == 2 {
                    intersections += 1;
                }
                x1 += dx;
                y1 += dy;
            }
        }
        intersections
    }

    fn unit_value(i: isize) -> isize {
        match i {
            _ if i < 0 => -1,
            _ if i > 0 => 1,
            _ => 0,
        }
    }
}