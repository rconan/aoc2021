use std::io;
use std::io::{BufRead, BufReader};
use anyhow::Result;
use aoc2021::syntax;

fn main() -> Result<()> {
    let mut input = Vec::new();
    io::stdin().lock().read_until(0, &mut input)?;
    let (syntax_score, autocomplete_score) = syntax::score_input(BufReader::new(&*input))?;
    println!("{}", syntax_score);
    println!("{}", autocomplete_score);
    Ok(())
}
