use std::io;
use anyhow::Result;
use aoc2021::lava::Cave;

fn main() -> Result<()> {
    let c = Cave::from_input(io::stdin().lock())?;
    println!("{}", c.risk_sum());
    println!("{}", c.basins());
    Ok(())
}
