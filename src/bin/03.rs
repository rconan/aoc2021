use std::io;
use anyhow::Result;
use aoc2021::submarine::Diagnostics;

fn main() -> Result<()> {
    let d = Diagnostics::from_input(&mut io::stdin().lock())?;
    let (gamma, epsilon) = d.rates();
    println!("{}", gamma * epsilon);
    let (oxygen, co2) = d.ratings();
    println!("{}", oxygen * co2);
    Ok(())
}