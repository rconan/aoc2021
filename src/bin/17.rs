use std::{cmp, io};
use anyhow::Result;
use aoc2021::trench::Trench;

fn main() -> Result<()> {
    let t = Trench::from_input(io::stdin().lock())?;
    let max_launch_x = t.max_x;
    let max_launch_y = t.min_y.abs();
    let mut peak_height = isize::MIN;
    let mut count = 0;
    for launch_x in 0..=max_launch_x {
        for launch_y in -max_launch_y..=max_launch_y {
            if let Some(h) = t.peak_height(launch_x, launch_y) {
                peak_height = cmp::max(peak_height, h);
                count += 1;
            }
        }
    }
    println!("{}", peak_height);
    println!("{}", count);
    Ok(())
}