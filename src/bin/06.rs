use std::io;
use anyhow::Result;
use aoc2021::fish::Shoal;

fn main() -> Result<()> {
    let mut s = Shoal::from_input(&mut io::stdin().lock())?;
    s.step_days(80);
    println!("{}", s.population());
    s.step_days(256 - 80);
    println!("{}", s.population());
    Ok(())
}