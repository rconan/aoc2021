use anyhow::Result;
use std::io;
use std::io::{BufRead, BufReader};

use aoc2021::submarine::Submarine;

fn main() -> Result<()> {
    let mut input = Vec::new();
    io::stdin().lock().read_until(0, &mut input)?;
    let mut s1 = Submarine::default();
    s1.navigate_from_input(BufReader::new(&*input))?;
    println!("{}", s1.multiplied_location());
    let mut s2 = Submarine::new(true);
    s2.navigate_from_input(BufReader::new(&*input))?;
    println!("{}", s2.multiplied_location());
    Ok(())
}