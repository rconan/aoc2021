use std::io;
use anyhow::Result;
use aoc2021::octopodi::Grid;

fn main() -> Result<()> {
    let mut g = Grid::from_input(io::stdin().lock())?;
    let mut part1 = None;
    let mut part2 = None;
    let mut i = 0;
    while part1 == None || part2 == None {
        i += 1;
        if g.step() == 100 && part2 == None {
            part2 = Some(i);
        }
        if i == 100 {
            part1 = Some(g.flashes());
        }
    }
    println!("{}", part1.unwrap());
    println!("{}", part2.unwrap());
    Ok(())
}
