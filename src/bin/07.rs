use std::io;
use anyhow::Result;
use aoc2021::crabs::{Crabs, Mode};

fn main() -> Result<()> {
    let c = Crabs::from_input(io::stdin().lock())?;
    println!("{}", c.alignment_fuel(Mode::Linear)?);
    let floor = c.alignment_fuel(Mode::IncrementalFloor)?;
    let ceil = c.alignment_fuel(Mode::IncrementalCeiling)?;
    println!("{}", floor.min(ceil));
    Ok(())
}