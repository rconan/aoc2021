use std::io;
use anyhow::Result;
use aoc2021::polymer::Polymer;

fn main() -> Result<()> {
    let mut p = Polymer::from_input(io::stdin().lock())?;
    for _ in 0..10 {
        p.step()?;
    }
    let (min, max) = p.frequency_range()?;
    println!("{}", max - min);
    for _ in 0..30 {
        p.step()?;
    }
    let (min, max) = p.frequency_range()?;
    println!("{}", max - min);
    Ok(())
}
