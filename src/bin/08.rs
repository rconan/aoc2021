use std::io;
use std::io::BufRead;
use anyhow::Result;
use aoc2021::display::Display;

fn main() -> Result<()> {
    let mut count_1478 = 0;
    let mut count_readings = 0;
    for l in io::stdin().lock().lines() {
        let line = l?;
        let mut parts = line.split(" | ");
        let display = Display::from_permutations_string(parts.next().unwrap())?;
        let reading_part = parts.next().unwrap();
        count_1478 += Display::count_1478(reading_part);
        count_readings += display.read(reading_part)?;
    }
    println!("{}", count_1478);
    println!("{}", count_readings);
    Ok(())
}
