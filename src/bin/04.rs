use anyhow::Result;
use std::io;
use aoc2021::bingo::Bingo;

fn main() -> Result<()> {
    let mut b = Bingo::from_input(&mut io::stdin().lock())?;
    println!("{}", b.play_to_winner()?);
    println!("{}", b.play_to_loser()?);
    Ok(())
}