use std::io;
use anyhow::Result;
use aoc2021::caves;

fn main() -> Result<()> {
    let s = caves::System::from_input(io::stdin().lock())?;
    println!("{}", s.path_count(false)?);
    println!("{}", s.path_count(true)?);
    Ok(())
}
