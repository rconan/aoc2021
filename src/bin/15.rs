use std::io;
use anyhow::Result;
use aoc2021::cave;

fn main() -> Result<()> {
    let m = cave::Map::from_input(io::stdin().lock())?;
    println!("{}", m.lowest_risk());
    let five = m.five_times();
    println!("{}", five.lowest_risk());
    Ok(())
}
