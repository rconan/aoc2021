use std::io;
use anyhow::Result;
use aoc2021::vents::VentMap;

fn main() -> Result<()> {
    let v = VentMap::from_input(io::stdin().lock())?;
    println!("{}", v.intersections(false));
    println!("{}", v.intersections(true));
    Ok(())
}