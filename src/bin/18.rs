use std::{cmp, io};
use std::io::BufRead;
use anyhow::Result;
use aoc2021::snailfish::Number;
use aoc2021::snailfish::Error;

fn main() -> Result<()> {
    let numbers = io::stdin().lock().lines().map(|l| Number::from_str(l?.as_str())).collect::<Result<Vec<Number>, Error>>()?;
    println!("{}", numbers.clone().into_iter().sum::<Number>().magnitude());
    let mut max = 0;
    for lhs in numbers.clone().into_iter() {
        for rhs in numbers.clone().into_iter() {
            max = cmp::max((lhs.clone() + rhs).magnitude(), max);
        }
    }
    println!("{}", max);
    Ok(())
}