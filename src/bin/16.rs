use std::io;
use anyhow::Result;
use aoc2021::bits;

fn main() -> Result<()> {
    let p = bits::Packet::from_input(io::stdin().lock())?;
    println!("{}", p.version_sum());
    println!("{}", p.value());
    Ok(())
}
