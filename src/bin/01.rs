use anyhow::Result;
use std::io;
use std::io::BufRead;
use aoc2021::depth::increase_count;

fn main() -> Result<()> {
    let stdin = io::stdin();
    let lines = stdin.lock().lines();
    let numbers = lines.map(|l| l.unwrap().parse().unwrap()).collect::<Vec<u32>>();
    println!("{}", increase_count(&mut numbers.clone().into_iter(), 1)?);
    println!("{}", increase_count(&mut numbers.clone().into_iter(), 3)?);
    Ok(())
}