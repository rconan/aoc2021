use std::io;
use anyhow::Result;
use aoc2021::origami;

fn main() -> Result<()> {
    let mut s = origami::Sheet::from_input(io::stdin().lock())?;
    s.do_fold()?;
    println!("{}", s.count_points());
    s.do_folds()?;
    s.pad()?;
    println!("{}", s.read_letters()?);
    Ok(()) //bump
}
