use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("insufficient input")]
    InsufficientInput(),
}

pub fn increase_count(inputs: &mut impl Iterator<Item=u32>, window_size: usize) -> Result<u32, Error> {
    let mut count = 0;
    let mut window = vec![0; window_size];
    let mut last = 0;
    for i in 0..window_size {
        window[i] = inputs.next().ok_or(Error::InsufficientInput())?;
        last += window[i]
    }
    for (i, next) in inputs.enumerate() {
        let window_position = i % window_size;
        let sum = last - window[window_position] + next;
        window[i % window_size] = next;
        if sum > last {
            count += 1;
        }
        last = sum;
    }
    Ok(count)
}
