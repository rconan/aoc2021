use std::io;
use std::num::ParseIntError;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error(transparent)]
    IOError(#[from] io::Error),
    #[error(transparent)]
    ParseIntError(#[from] ParseIntError),
    #[error("Insufficient Input")]
    InsufficientInput,
}

#[derive(Default, Debug)]
pub struct Shoal {
    counts: [usize; 9],
}

impl Shoal {
    pub fn from_input(input: impl io::BufRead) -> Result<Self, Error> {
        let mut s = Self::default();
        for f in input.lines().next().ok_or(Error::InsufficientInput)??.split(',').map(|t| t.parse::<usize>()) {
            s.counts[f?] += 1;
        }
        Ok(s)
    }

    fn step_day(&mut self) {
        let breeders = self.counts[0];
        for i in 0..8 {
            self.counts[i] = self.counts[i+1];
        }
        self.counts[8] = breeders;
        self.counts[6] += breeders;
    }

    pub fn step_days(&mut self, n: usize) {
        for _ in 0..n {
            self.step_day();
        }
    }

    pub fn population(&self) -> usize {
        self.counts.iter().sum()
    }
}