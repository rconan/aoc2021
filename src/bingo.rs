use std::{
    io,
    num::ParseIntError,
};
use fxhash::FxHashMap;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error(transparent)]
    IOError(#[from] io::Error),
    #[error(transparent)]
    ParseIntError(#[from] ParseIntError),
    #[error("Need at least 1 line of input")]
    InsufficientInput,
    #[error("Incomplete board, have {0} rows, need 5")]
    IncompleteBoard(usize),
    #[error("Nobody won")]
    NoWinner,
}

#[derive(Default)]
struct Board {
    numbers: FxHashMap<usize, (usize, usize)>,
    columns: [usize; 5],
    rows: [usize; 5],
}

impl Board {
    fn from_input(lines: &mut impl Iterator<Item=io::Result<String>>) -> Result<Self, Error> {
        let mut b = Self{
            numbers: FxHashMap::default(),
            columns: [0; 5],
            rows: [0; 5],
        };
        let mut rows = 0;
        for line in lines {
            match line?.as_str() {
                "" => {},
                s => {
                    for (i, n) in s.split_whitespace().map(|t| t.parse::<usize>()).enumerate() {
                        b.numbers.insert(n?, (i, rows));
                    }
                    rows += 1;
                },
            };
            if rows == 5 {
                return Ok(b)
            }
        }
        Err(Error::IncompleteBoard(rows))
    }

    fn call(&mut self, n: usize) -> Option<usize> {
        match self.numbers.remove(&n) {
            None => {},
            Some((i, j)) => {
                self.columns[i] += 1;
                self.rows[j] += 1;
                if self.columns[i] == 5 || self.rows[j] == 5 {
                    return Some(self.score(n))
                }
            }
        }
        return None
    }

    fn score(&self, n: usize) -> usize {
        self.numbers.keys().sum::<usize>() * n
    }
}

#[derive(Default)]
pub struct Bingo {
    boards: Vec<Board>,
    draws: Vec<usize>,
}

impl Bingo {
    pub fn from_input(input: impl io::BufRead) -> Result<Self, Error> {
        let mut b = Self::default();
        let mut lines = input.lines().peekable();
        for d in lines.next().ok_or(Error::InsufficientInput)??.split(',').map(|d| d.parse::<usize>()) {
            b.draws.push(d?);
        }
        while lines.peek().is_some() {
            b.boards.push(Board::from_input(&mut lines)?);
        }
        Ok(b)
    }

    pub fn play_to_winner(&mut self) -> Result<usize, Error> {
        for n in &self.draws {
            for b in self.boards.iter_mut() {
                match b.call(*n) {
                    Some(s) => { return Ok(s) },
                    None => {},
                }
            }
        }
        return Err(Error::NoWinner)
    }

    pub fn play_to_loser(&mut self) -> Result<usize, Error> {
        let mut result = Err(Error::NoWinner);
        for n in &self.draws {
            let mut winners = vec![];
            for (i, b) in self.boards.iter_mut().enumerate() {
                match b.call(*n) {
                    Some(s) => {
                        result = Ok(s);
                        winners.push(i);
                    },
                    None => {},
                }
            }
            for winner in winners.iter().rev() {
                self.boards.remove(*winner);
            }
        }
        result
    }
}