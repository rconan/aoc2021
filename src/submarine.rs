use std::io::BufRead;
use pest::Parser;
use pest_derive::Parser;
use std::io;
use std::num::ParseIntError;
use thiserror::Error;

#[derive(Parser)]
#[grammar = "submarine.pest"]
struct CourseParser;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Internal Error")]
    InternalError,
    #[error(transparent)]
    IOError(#[from] io::Error),
    #[error(transparent)]
    ParseIntError(#[from] ParseIntError),
    #[error(transparent)]
    ParseError(#[from] pest::error::Error<Rule>),
    #[error("Need at least 1 line of input")]
    InsufficientInput,
    #[error("Found mismatched line lengths")]
    MismatchedLength,
    #[error("Found unexpected character: {0}")]
    UnexpectedCharacter(char),
    #[error("Bit position {0} has equal count")]
    EqualCount(usize),
}

#[derive(Default)]
pub struct Submarine {
    aim_enabled: bool,
    aim: u32,
    depth: u32,
    position: u32,
}

impl Submarine {
    pub fn new(aim: bool) -> Self {
        return Self{aim_enabled: aim, aim: 0, depth: 0, position: 0}
    }

    pub fn multiplied_location(&self) -> u32 {
        self.position * self.depth
    }

    pub fn navigate_from_input(&mut self, input: impl BufRead) -> Result<(), Error> {
        for line in input.lines() {
            self.execute_input_line(line?.as_str())?;
        }
        Ok(())
    }

    fn execute_input_line(&mut self, input: &str) -> Result<(), Error> {
        let mut pairs = CourseParser::parse(Rule::command, input)?.next().ok_or(Error::InternalError)?.into_inner();
        let direction_pair = pairs.next().ok_or(Error::InternalError)?;
        let distance_pair = pairs.next().ok_or(Error::InternalError)?;
        let distance: u32 = distance_pair.as_str().parse()?;
        match direction_pair.as_str() {
            "forward" => self.forward(distance),
            "up" => self.up(distance),
            "down" => self.down(distance),
            &_ => Err(Error::InternalError),
        }
    }

    fn forward(&mut self, distance: u32) -> Result<(), Error> {
        self.position += distance;
        if self.aim_enabled {
            self.depth += self.aim * distance
        }
        Ok(())
    }

    fn up(&mut self, distance: u32) -> Result<(), Error> {
        if self.aim_enabled {
            self.aim -= distance;
        } else {
            self.depth -= distance;
        }
        Ok(())
    }

    fn down(&mut self, distance: u32) -> Result<(), Error> {
        if self.aim_enabled {
            self.aim += distance;
        } else {
            self.depth += distance;
        }
        Ok(())
    }
}

#[derive(Debug)]
pub struct Diagnostics {
    bit_count: usize,
    values: Vec<usize>,
}

impl Diagnostics {
    pub fn from_input(input: &mut impl BufRead) -> Result<Self, Error> {
        let mut lines = input.lines().peekable();
        let first_line = lines.peek().ok_or(Error::InsufficientInput)?.as_ref().unwrap();
        let bit_count = first_line.len();
        let mut d = Self{
            bit_count,
            values: Vec::new(),
        };
        for res in lines {
            let line = res?;
            if line.len() != bit_count {
                return Err(Error::MismatchedLength)
            }
            d.values.push(usize::from_str_radix(line.as_str(), 2)?);
        }
        Ok(d)
    }

    fn mode_for_bit(values: &Vec<usize>, i: usize) -> usize {
        let ones_count = values.iter().map(|v| v >> i & 1).sum::<usize>();
        if 2 * ones_count >= values.len() { 1 } else { 0 }
    }

    pub fn rates(&self) -> (usize, usize) {
        let mut gamma = 0;
        for i in 0..self.bit_count {
            if Self::mode_for_bit(&self.values, i) == 1 {
                gamma |= 1 << i;
            }
        }
        (gamma, !gamma & ((1 << self.bit_count) - 1))
    }

    fn oxygen_generator_rating(candidates: &Vec<usize>, length: usize) -> usize {
        match candidates.len() {
            1 => candidates[0],
            _ => {
                let msb_pos = length - 1;
                let mut new_candidates = Vec::new();
                let mode_msb = Self::mode_for_bit(candidates, msb_pos);
                for c in candidates {
                    if (c >> msb_pos) == mode_msb {
                        new_candidates.push(c & (1 << msb_pos) - 1);
                    }
                }
                (mode_msb << msb_pos) | Self::oxygen_generator_rating(&new_candidates, msb_pos)
            },
        }
    }

    fn co2_scrubber_rating(candidates: &Vec<usize>, length: usize) -> usize {
        match candidates.len() {
            1 => candidates[0],
            _ => {
                let msb_pos = length - 1;
                let mut new_candidates = Vec::new();
                let not_mode_msb = Self::mode_for_bit(candidates, msb_pos) ^ 1;
                for c in candidates {
                    if (c >> msb_pos) == not_mode_msb {
                        new_candidates.push(c & (1 << msb_pos) - 1);
                    }
                }
                (not_mode_msb << msb_pos) | Self::co2_scrubber_rating(&new_candidates, msb_pos)
            },
        }
    }

    pub fn ratings(&self) -> (usize, usize) {
        (Self::oxygen_generator_rating(&self.values, self.bit_count),Self::co2_scrubber_rating(&self.values, self.bit_count))
    }
}