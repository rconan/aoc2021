use std::io;
use std::io::BufRead;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error {
    #[error(transparent)]
    IOError(#[from] io::Error),
}

#[derive(Debug,Eq,PartialEq)]
pub enum Packet {
    Value {
        version: usize,
        value: usize,
    },
    Operator {
        version: usize,
        operation: usize,
        packets: Vec<Self>,
    },
}

impl Packet {
    pub fn from_input(mut input: impl BufRead) -> Result<Self, Error> {
        let mut line = String::new();
        input.read_line(&mut line)?;
        let (p, _) = Self::from_bits(&mut line.chars().map(|c| {
            let d = c.to_digit(16).unwrap();
            let arr = [
                d >> 3 & 1 == 1,
                d >> 2 & 1 == 1,
                d >> 1 & 1 == 1,
                d & 1 == 1,
            ];
            arr
        }).flatten())?;
        Ok(p)
    }

    pub fn from_bits(input: &mut dyn Iterator<Item=bool>) -> Result<(Self, usize), Error> {
        let version = Self::read_value(input, 3);
        Ok(match Self::read_value(input, 3) {
            4 => {
                let mut value = 0;
                let mut cont = true;
                let mut length = 6;
                while cont {
                    cont = input.next().unwrap();
                    value *= 16;
                    value += Self::read_value(input, 4);
                    length += 5;
                }
                (
                    Self::Value {
                        version,
                        value,
                    },
                    length,
                )
            },
            operator => {
                let mut packets = Vec::new();
                let mut length = 7;
                match input.next().unwrap() {
                    false => {
                        length += 15;
                        let target_length = length + Self::read_value(input, 15);
                        while length < target_length {
                            let (p, l) = Self::from_bits(input)?;
                            packets.push(p);
                            length += l;
                        }
                    },
                    true => {
                        length += 11;
                        let target_count = Self::read_value(input, 11);
                        for _ in 0..target_count {
                            let (p, l) = Self::from_bits(input)?;
                            packets.push(p);
                            length += l;
                        }
                    },
                }
                (
                    Self::Operator {
                        version,
                        operation: operator,
                        packets,
                    },
                    length,
                )
            },
        })
    }

    pub fn version_sum(&self) -> usize {
        match self {
            Self::Value{version, ..} => *version,
            Self::Operator{version, packets, ..} => *version + packets.iter().map(|p| p.version_sum()).sum::<usize>(),
        }
    }

    pub fn value(&self) -> usize {
        match self {
            Self::Value{value, ..} => *value,
            Self::Operator{operation, packets, ..} => match operation {
                0 => packets.iter().map(|p| p.value()).sum::<usize>(),
                1 => packets.iter().map(|p| p.value()).product::<usize>(),
                2 => packets.iter().map(|p| p.value()).min().unwrap(),
                3 => packets.iter().map(|p| p.value()).max().unwrap(),
                5 => if packets.len() < 2 || packets[0].value() <= packets[1].value() { 0 } else { 1 },
                6 => if packets.len() < 2 || packets[0].value() >= packets[1].value() { 0 } else { 1 },
                7 => if packets.len() < 2 || packets[0].value() != packets[1].value() { 0 } else { 1 },
                _ => 0,
            }
        }
    }

    fn read_value(input: &mut dyn Iterator<Item=bool>, bits: usize) -> usize {
        let mut value = 0;
        for bit in input.take(bits) {
            value *= 2;
            value += bit as usize;
        }
        value
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_value_from_bits() -> Result<(), Error> {
        let input: Vec<bool> = vec![
            true,true,false,
            true,false,false,
            true,false,true,true,true,
            true,true,true,true,false,
            false,false,true,false,true,
        ];
        let (packet, length) = Packet::from_bits(&mut input.into_iter())?;
        assert_eq!(packet, Packet::Value{
            version: 6,
            value: 2021,
        });
        assert_eq!(length, 21);
        Ok(())
    }

    #[test]
    fn test_operator_with_length_from_bits() -> Result<(), Error> {
        let input: Vec<bool> = vec![
            false,false,true,
            true,true,false,
            false,
            false,false,false,false,false,false,false,false,false,false,true,true,false,true,true,
            true,true,false,true,false,false,false,true,false,true,false,
            false,true,false,true,false,false,true,false,false,false,true,false,false,true,false,false,
        ];
        let (packet, length) = Packet::from_bits(&mut input.into_iter())?;
        assert_eq!(packet, Packet::Operator{
            version: 1,
            operation: 6,
            packets: vec![Packet::Value{version: 6, value: 10}, Packet::Value{version: 2, value: 20}],
        });
        assert_eq!(length, 49);
        Ok(())
    }

    #[test]
    fn test_operator_with_count_from_bits() -> Result<(), Error> {
        let input: Vec<bool> = vec![
            true,true,true,
            false,true,true,
            true,
            false,false,false,false,false,false,false,false,false,true,true,
            false,true,false,true,false,false,false,false,false,false,true,
            true,false,false,true,false,false,false,false,false,true,false,
            false,false,true,true,false,false,false,false,false,true,true,
        ];
        let (packet, length) = Packet::from_bits(&mut input.into_iter())?;
        assert_eq!(packet, Packet::Operator{
            version: 7,
            operation: 3,
            packets: vec![Packet::Value{version: 2, value: 1}, Packet::Value{version: 4, value: 2}, Packet::Value{version: 1, value: 3}],
        });
        assert_eq!(length, 51);
        Ok(())
    }
}