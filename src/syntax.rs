use std::io;
use std::io::BufRead;
use thiserror::Error;

#[derive(Error,Debug)]
pub enum Error {
    #[error(transparent)]
    IOError(#[from] io::Error)
}

fn score_line(line: String) -> (bool, usize) {
    let mut stack = Vec::new();
    for c in line.chars() {
        if stack.last() == Some(&c) {
            stack.pop();
            continue
        }
        match c {
            '(' => stack.push(')'),
            '[' => stack.push(']'),
            '{' => stack.push('}'),
            '<' => stack.push('>'),
            ')' => return (true, 3),
            ']' => return (true, 57),
            '}' => return (true, 1197),
            '>' => return (true, 25137),
            _ => continue,
        }
    }
    let mut autocomplete_score = 0;
    for c in stack.iter().rev() {
        autocomplete_score *= 5;
        autocomplete_score += match *c {
            ')' => 1,
            ']' => 2,
            '}' => 3,
            '>' => 4,
            _ => panic!("Unexpected character on the stack"),
        }
    }
    (false, autocomplete_score)
}

pub fn score_input(input: impl BufRead) -> Result<(usize, usize), Error> {
    let mut syntax_score = 0;
    let mut autocomplete_scores = Vec::new();
    for line in input.lines() {
        let (error, score) = score_line(line?);
        if error {
            syntax_score += score
        } else {
            autocomplete_scores.push(score)
        }
    }
    autocomplete_scores.sort();
    Ok((syntax_score, autocomplete_scores[autocomplete_scores.len() / 2]))
}
